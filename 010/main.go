package main

import (
    "fmt"
)

func check_if_prime(x int) (bool) {

	if (x <= 1) {
		return (false)
	} else if (x <= 3) {
		return true
	} else if (x % 2 == 0 || x % 3 == 0) {
		return false
	}

	var i int = 5
	
	for (i*i <= x) {
		if (x % i == 0 || x % (i + 2) == 0) {
			return false
		}
		i += 6
	}
	return true
}

func main() {

	var res int = 0

	for i := 0; i < 2000000; i++ {
		if (check_if_prime(i) == true) {
			res += i
		}
	}
	fmt.Printf("Res = %d", res)
}
