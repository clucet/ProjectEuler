package main

import (
    "fmt"
	"sort"
	//"math"
)

func isTriangular(number int) (bool) {

	if number < 0 {
		return false
	}

	var sum int = 0
	var n int = 1

	for (sum <= number) {
		sum += n
		if (sum == number) {
			return true
		}
		fmt.Printf("sum = %d \n\n", sum)
		n += 1
	}
	return false
}

func main() {

	res := make([]int, 7)

	for i := 1; i <= 30; i++ {
		if isTriangular(i) {
			res = append(res, i)
		}
	}
	sort.Ints(res)
	fmt.Println(res)
}
