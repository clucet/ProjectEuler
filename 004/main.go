package main

import (
    "fmt"
    "sort"
)

func reverse_number(num int) (int) {
    new_int := 0
    for num > 0 {
        remainder := num % 10
        new_int *= 10
        new_int += remainder
        num /= 10
    }
    return new_int
}

func main() {

    var res []int
    for i := 100; i  <= 999; i++ {
        for j := 100; j <= 999; j++ {
            tmp := i * j
            if (reverse_number(tmp) == tmp) {
                res = append(res, tmp)
            }
        }
    }
    sort.Ints(res)
    fmt.Printf("The largest palindrom made from the product of two 3-digit numbers is : %d\n", res[len(res)-1])
}
