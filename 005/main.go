package main

import (
    "fmt"
)

func bool_check(num int64) (res bool) {

    var tmp int64 = 11

    for (tmp <= 20) {
        if (num % tmp != 0) {
            return false
        }
        tmp++
    }
    return true
}

func main() {

    var res int64 = 20

    for (bool_check(res) == false) {
        res += 20
    }
    fmt.Printf("%d \n", res)
}
