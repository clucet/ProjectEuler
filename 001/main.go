package main

import (
    "fmt"
)

func main() {

    var res int = 0

    for n := 0; n < 1000; n++ {
        if (n % 3 == 0 || n % 5 == 0) {
            res += n
        }
    }
    fmt.Printf("res = %d \n", res)
}
