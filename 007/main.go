package main

import (
    "fmt"
	"sort"
	//"math"
)

func isIntegral(val float64) bool {
	return val == float64(int(val))
}

func main() {

	var prime_number float64 = 2
	var j float64 = 2
	m := make(map[int]float64)
	var count int = 0

	for i := 1; i <= 200000; i++ {
		for prime_number <= 200000 {
			for j <= 200000 {
				if (isIntegral(prime_number / j) && count == 0) {
					if (prime_number / j == 1) {
						m[i] = prime_number
						i++
					}
					count++
				}
			j++
			}
		j = 2
		count = 0
		prime_number += 1
		}
	}

	//
	//	Sorting my maps
	//
	var keys []int
	for k := range m {
		keys = append(keys, k)
	}
	sort.Ints(keys)


	fmt.Println("10001 prime number is : ", m[10001], "\n")

}
