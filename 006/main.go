package main

import (
    "fmt"
	"math"
)

func sum_of_squares() (res float64) {

	var i float64 = 1

	for i <= 100 {
		res += math.Pow(i, 2)
		i += 1
	}
	return res
}

func square_of_the_sum() (res float64) {

	var i float64 = 1

	for i <= 100 {
		res += i
		i++
	}
	return (math.Pow(res, 2))
}

func main() {

	var sum_of_squares float64 = sum_of_squares()
	var square_of_the_sum float64 = square_of_the_sum()

	fmt.Printf("sum of squares = %f \n", sum_of_squares)
	fmt.Printf("squares of the sum= %f \n", square_of_the_sum)
	fmt.Printf("squares of the sum - sum of squares = %f \n", square_of_the_sum - sum_of_squares)
}
