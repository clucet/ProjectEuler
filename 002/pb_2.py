#!/usr/local/bin/python3

def fibonacci(n):
	if n == 1 or n == 2:
		return 1
	else:
		return (fibonacci(n - 1) + fibonacci(n - 2))

if __name__ == '__main__':
	res = 0

	for x in range(2, 34):
		tmp = fibonacci(x)
		if (tmp % 2 == 0):
			res += tmp
			print('Fibonaci de {:d} = {:d}'.format(x, tmp))
	print('Res = {:d}'.format(res))
