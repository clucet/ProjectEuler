package main

import (
    "fmt"
)

func    fibonacci(num int) (int) {

    if (num == 1 || num == 2) {
        return (1)
    } else {
        return (fibonacci(num - 1) + fibonacci(num - 2))
    }
}

func    main() {

    var res int = 0

    for x := 2; x < 34; x++ {
        tmp := fibonacci(x)
        if (tmp % 2 == 0) {
            res += tmp
            fmt.Printf("Fibonacci de %d = %d \n", x, tmp)
        }
    }
    fmt.Printf("Res == %d \n", res)
}
