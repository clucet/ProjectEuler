#!/usr/local/bin/python3

import argparse
from fractions import gcd

def	prime(n):
	a = []
	f = 2

	while n > 1:
		if (n % f == 0):
			a.append(f)
			n /= f
		else:
			f += 1
	return a

def pollards_rho(n):
	x = 2; y = 2; d = 1
	f = lambda x: (x**2 + 1) % n
	while d == 1:
		x = f(x); y = f(f(y))
		d = gcd(abs(x-y), n)
	print(d)
	if d != n:
		return d

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("Num", help="The number you want to see prime factorized", type=int)
	args = parser.parse_args()

	##print('Num = {:d}'.format(args.Num))
	#mylist = prime(args.Num)
	n = args.Num
	p = pollards_rho(n)
	p = pollards_rho(n)
	print(p)	
	print(p)
	#print('{} = {} * {}'.format(n, p, res))

#print(*mylist, sep=', ')
